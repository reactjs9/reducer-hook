import React, {useEffect, useReducer} from 'react'

const reducer = (state, action) => {
    switch (action.type) {
        case 'USERS_REQUEST':
            return {...state, loading: true};
        case 'USERS_SUCCESS':
            return {...state, users: action.payload, loading: false};
        case 'USERS_FAIL':
            return {...state,error: action.payload, loading: false};
        default:
            return state;

    }
}

export default function UserList() {
    const [state, dispatch] = useReducer(reducer, {
        loading: false, users: [], error:''
    });
    const {loading, users, error} = state;
    const loadUsers = async ()=>{
        dispatch({type: "USERS_REQUEST"});
        try{
            const response = await fetch("https://jsonplaceholder.typicode.com/users");
            const data = await response.json();
            dispatch({type: "USERS_SUCCESS", payload: data});
            
        }catch(error){
            dispatch({type: "USERS_FAIL", payload: error.message});
        }
    }

    useEffect(()=>{
        loadUsers();
    }, [])
  return (
    <div className="userList">
        {
            loading ? <div>Loading users...</div> : 
            ( error ? <h2>{error.message}</h2> : 
              <table>
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>User Name</th>
                          <th>Email</th>
                          <th>Address</th>
                      </tr>
                  </thead>
                  <tbody>
                     {users.map(user=>{
                        return <tr key={user.id}>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.email}</td>
                            <td>{user.address?.street}</td>
                        </tr>
                     })}
                      
                  </tbody>
                  
              </table>  
            )
        }
    </div>
  )
}
